// Based on the example from the library. -hsin

var SCREEN_WIDTH = window.innerWidth,
SCREEN_HEIGHT = window.innerHeight,

camera, scene, renderer, radians;

init();
animate();

function init() {

	var container;

	container = document.getElementById("hyper");

	camera = new THREE.Camera( 75, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 10000 );
	camera.position.z = 1000;

	scene = new THREE.Scene();

	radians = 0;

	renderer = new THREE.SVGRenderer();
	renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	container.appendChild(renderer.domElement);


	for (var i = 0; i < 300; i++) {

		// stubble

		var geometry = new THREE.Geometry();

		var vector = new THREE.Vector3( Math.random() * 2 - 1, Math.random() * 2 - 1, Math.random() * 2 - 1 );
		vector.normalize();
		vector.multiplyScalar( 450 );

		geometry.vertices.push( new THREE.Vertex( vector ) );

		var vector2 = vector.clone();
		vector2.multiplyScalar( 1.01 );

		geometry.vertices.push( new THREE.Vertex( vector2 ) );

		var line = new THREE.Line( geometry, new THREE.LineBasicMaterial( { color: 0xffffff, opacity: Math.random() } ) );
		scene.addObject( line );

		// porcupine

		var unitz = new THREE.Vector3( 0, 0, 1 );
		var geometry2 = new THREE.Geometry();
		var shift = new THREE.Vector3( 0, 0, 600 );

		var vector3 = vector.clone();
		if ( vector3.dot( unitz ) > 0 ) {

			// only pointing away (+z) from the stubby sphere
			vector3.divideScalar( 5 );
			vector3.add( vector3, shift );

			geometry2.vertices.push( new THREE.Vertex( vector3 ) );

			var vector4 = vector.clone();
			vector4.multiplyScalar( 1.3 );
			vector4.add( vector4, shift );

			geometry2.vertices.push( new THREE.Vertex( vector4 ) );

			var line2 = new THREE.Line( geometry2, new THREE.LineBasicMaterial( { color: 0xffffff, opacity: Math.random() } ) );
			scene.addObject( line2 );
		}
	}
}

//

function animate() {

	requestAnimationFrame( animate );

	render();

}

function render() {

	if ( camera.position.z > 450 ) {

		// zooming
		camera.position.z += -4;

	} else if ( camera.position.z < 200 ) {

		// hover in a circle
		camera.position.x = Math.cos( radians );
		camera.position.y = Math.sin( radians );
		radians = radians + Math.PI/180

	} else {

		// reached stubble
		camera.position.z += -0.08;

	}

	renderer.render( scene, camera );
}
