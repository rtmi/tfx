Ext.ux.Raphael = Ext.extend(Ext.BoxComponent, {
    onRender: function(ct) {
        var p = this.paper = Raphael(ct.dom), v;
        this.el = Ext.get(p.canvas);

//      Export all methods from this paper object which will not override our native
//      methods like setSize etc.
        for (var prop in p) {
            v = p[prop];
            if (!this[prop] && Ext.isFunction(v)) {
                this[prop] = v.createDelegate(p);
            }
        }

//      We always cache our size
        this.cacheSizes = true;
    },

    getWidth: function() {
        return this.lastSize.width;
    },

    getHeight: function() {
        return this.lastSize.height;
    },

    onResize: function(w, h) {
        this.paper.setSize(w, h);
    }
});
Ext.reg('raphael', Ext.ux.Raphael);

Ext.onReady(function () {
    var r = new Ext.ux.Raphael(),
        sectorsCount = 12,
        color = "#000",
        width = 15,
        r1 = 35,
        r2 = 60,
        cx = 200,
        cy = 200,
        sectors = [],
        opacity = [],
        beta = 2 * Math.PI / sectorsCount,
        pathParams = { stroke: color, "stroke-width": width, "stroke-linecap": "round" };

    var spinner = new Ext.Window({
        x: 20, y: 20,
        title: 'Loading...',
        height: 432,
        width: 414,
        layout: 'fit',
        items: r
    });
    spinner.show();

    //  Keep spinner in centre
    r.on('resize', function () {
        var ncx = r.getWidth() / 2;
        var ncy = r.getHeight() / 2;
        for (var i = 0; i < sectorsCount; i++) {
            sectors[i].translate(ncx - cx, ncy - cy);
        }
        cx = ncx;
        cy = ncy;
    });

    for (var i = 0; i < sectorsCount; i++) {
        var alpha = beta * i - Math.PI / 2,
        cos = Math.cos(alpha),
        sin = Math.sin(alpha);
        opacity[i] = 1 / sectorsCount * i;
        sectors[i] = r.path([["M", cx + r1 * cos, cy + r1 * sin], ["L", cx + r2 * cos, cy + r2 * sin]]).attr(pathParams);
        if (color == "rainbow") {
            sectors[i].attr("stroke", Raphael.getColor());
        }
    }

    (function ticker() {
        opacity.unshift(opacity.pop());
        for (var i = 0; i < sectorsCount; i++) {
            sectors[i].attr("opacity", opacity[i]);
        }
        r.safari();
        setTimeout(ticker, 1000 / sectorsCount);
    })();

    //todo pause and display puzzle, then banish spinner; may need callback.
    var p = new Puzzle();
    p.newPuzzle();
    var ls = p.getArray();
		var rs = new Ext.ux.Raphael();
    new Ext.Window({
        title: 'Squares',
        height: 500,
        width: 500,
        layout: 'fit',
				closable: false,
        items: rs
    }).show();
    function attachCellhandler(paper, cells) {
        var process = function (j) {
            var box = paper.rect(cells[j].col*40-20, cells[j].row*40-20,40,40,6).attr({'gradient': '90-#526c7a-#64a0c1', 'stroke': '#3b4449', 'stroke-width': 1});
						var tip = paper.text(cells[j].col*40, cells[j].row*40, cells[j].typeid.toString()).attr({'font-size':40, 'font-family': 'Times, Arial', 'fill': '#ff8000', opacity:0});
						tip.toBack();

            box.node.onmouseover= function () {
                //box.animate({ scale:[1.1,1.1,cells[j].col*40-20, cells[j].row*40-20]},500,"elastic");
								box.attr({'cursor': 'pointer', 'stroke': '#ffff00', 'stroke-width': 2});
            };
            box.node.onmouseout = function () {
                //box.animate({ scale:[1,1,cells[j].col*40-20, cells[j].row*40-20]},500,"elastic");
								box.attr({'cursor': 'default', 'stroke': '#3b4449', 'stroke-width': 1});
            };
            box.node.onclick = function () {
                tip.animate({ opacity: 1 }, 1000);
                box.animate({ opacity: 0 }, 1000, function () {
                this.remove();
                });
            };
        }

        for (var i = 0; i < cells.length; i++) {
            process(i);
        }
				paper.safari();
    };
    attachCellhandler(rs, ls);
});

function Puzzle() {
    var _pool = new Pool();
    var _context = new TileStack();

    function guessTile(startcell) {
        _pool.restart();
        var fails = null;
        if (startcell != null) {
            fails = startcell.fails;
            fails.push(startcell.typeid);
        }
        while (_pool.length > 0) {
            var cardval = _pool.shuffle(fails);
            if (cardval == 0) { break; }

            var t = _context.nextTile(cardval, fails);
            if (!_context.simpleMatch(t)) {
                return t;
            }
        }
        return null;
    }
    this.newPuzzle = function () {
        _context.initialize();
        var lastid = 0;
        var startcell = null;
        while (!_context.isComplete()) {
            var t = guessTile(startcell);
            if (t != null) {
                startcell = null;
                _context.add(t);
            }
            else {
                startcell = _context.discard();
                if (startcell != null && startcell.sortid == lastid) {
                    startcell = _context.discard();
                }
                lastid = 0;
                if (startcell != null) {
                    lastid = startcell.sortid;
                }
            }
        }
    }
    this.getArray = function () {
				/*var t =[];
        for (var i = 0; i < _context.list.length; i++) {
						t.push(_context.list[i].typeid);
        }
				console.debug(t.toString());*/
				
        return _context.list;
    }
}

function TileStack() {
    this.list = [];
    this.nextid = 0;
}
TileStack.prototype.simpleMatch = function (t) {
    for (var i = 0; i < this.list.length; i++) {
        if (this.list[i] == null) { continue; }
        if (this.list[i].typeid != t.typeid) { continue; }

        if (this.list[i].col == t.col ||
            this.list[i].row == t.row ||
            this.list[i].mini == t.mini) {
            return true;
        }
    }
    return false;
}
TileStack.prototype.discard = function () {
    var t = null;
		var txt = "discard:[";
    for (var i = this.list.length - 1; i > 8; i--) {
        this.nextid = this.list[i - 1].sortid;
        t = this.list.pop();
				txt += t.sortid +",";
        if (t != null && t.starter) { break; }
        else { t = null; }
    }
		txt += "]\n";
		//console.debug(txt);
		if (t!=null && t.starter){
			var ftxt=t.sortid + ":fails[";
			for(var i=0;i<t.fails.length;i++){
				ftxt += t.fails[i] + ", ";
			}
			ftxt += "]\n";
			//console.debug(ftxt);
		}
    return t;
}
TileStack.prototype.initialize = function () {
		var tmp = [1,2,3,4,5,6,7,8,9];
    this.list = [];

		while(tmp.length >0) {
			var i = Math.floor((Math.random()*tmp.length));

			this.list.push(new Tile(this.list.length+1,tmp[i],null));

			tmp.splice(i,1);
		}
    this.nextid = 9;
}
TileStack.prototype.add = function (t) {
    this.list.push(t);
    this.nextid = t.sortid;
}
TileStack.prototype.nextTile = function (v, fails) {
    return new Tile(this.nextid + 1, v, fails);
}
TileStack.prototype.isComplete = function() {
return this.nextid == 81;
}

function Pool() {
    this.list = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    this.head = 0;
    this.length = 0;
}
Pool.prototype.restart = function() {
    this.length = this.list.length;
    this.head = (this.head +1)%9;
}
Pool.prototype.shuffle = function (exclude) {
    while (this.length > 0) {
        var i = this.head;
        this.length = this.length - 1;
        this.head = (this.head + 1) % 9;
        if (exclude == null || exclude.indexOf(this.list[i])==-1) {
            return this.list[i];
        }
    }
    return 0;
}

function Tile(i, v, f) {
    this.col = CalcColumn(i);
    this.row = CalcRow(i);
    this.mini = CalcMini(this.col,this.row);
    this.sortid = i;
    this.typeid = v;
    this.starter = CalcStarter(this.col);
    this.fails = CalcFails(this.col,f);

    function CalcColumn(i) {
        var m = i % 9;
        if (m == 0) { return 9; }
        else { return m; }
    }

    function CalcRow(i) {
        var m = Math.floor(i / 9);
        if (i % 9 == 0) { return m; }
        else { return m + 1; }
    }

    function CalcMini(col,row) {
        var x = Math.floor(col / 3);
        var y = Math.floor(row / 3);
        var s = "";
        if (col % 3 == 0) { s += x.toString(); }
        else { s += (x + 1).toString(); }
        if (row % 3 == 0) { s += y.toString(); }
        else { s += (y + 1).toString(); }
        return s;
    }

    function CalcStarter(col) {
        if (col == 1) { return true; }
        return false;
    }

    function CalcFails(col,f) {
        if (f != null && col == 1) {
            return f.slice();
        }
        else {
            return [];
        }
    }
}
