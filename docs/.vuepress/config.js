const { description } = require('../../package')

module.exports = {

  title: 'Always wrong, sometimes lucky',
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#5e2ca5' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['link', { rel: 'icon', type: 'image/svg+xml', href: '/img/favicon.svg' }]
  ],

  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    search: false,
    nav: [
      {
        text: 'Easiest',
        link: '/ezunbound/',
      },
      {
        text: 'Modify',
        link: '/sinkhole/',
      },
      {
        text: 'DIY',
        link: '/diyraspi/',
      }
    ],

    sidebar: {
      '/ezunbound/': [
        {
          title: 'Easiest',
          collapsable: false,
          children: [
            '',
          ]
        }
      ],
      '/sinkhole/': [
        {
          title: 'Modify',
          collapsable: false,
          children: [
            '',
          ]
        }
      ],
      '/diyraspi/': [
        {
          title: 'DIY',
          collapsable: false,
          children: [
            '',
            'subsys',
            'build',
          ]
        }
      ],
    }
  },

  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]

}
