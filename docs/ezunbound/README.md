# Unbound

## Easy Docker, Happy Life

For the easiest path to block ads, seeing is believing. So the best
 demonstration is getting things running quickly.

1. Docker makes this painless. Go grab [Docker](https://docs.docker.com/get-docker/).

2. Fetch the list of domains to block:

```bash
curl -o unbound.bl https://raw.githubusercontent.com/oznu/dns-zone-blacklist/master/unbound/unbound.blacklist

```

3. Fetch the DNS root servers cache:

```bash
curl -o root.hints https://www.internic.net/domain/named.root

```

4. Use root hints and block list in the Unbound configuration file:

```bash
sudo nano unbound.conf
```

With file contents:
```yaml
server:
    interface: 0.0.0.0
    port: 5300

    do-ip6: no
    do-daemonize: no

    access-control: 127.0.0.1/32 allow
    access-control: 192.168.0.0/16 allow
    access-control: 172.16.0.0/12 allow
    access-control: 10.0.0.0/8 allow

    logfile: ""
    verbosity: 5

    root-hints: /opt/unbound/etc/unbound/root.hints
    include: /opt/unbound/etc/unbound/unbound.bl

```
Exit and save changes.

5. Run [Kyle Harding's Unbound image](https://gitlab.com/klutchell/unbound) 

```bash
docker run --name unbound -p 5300:5300/udp -v $(pwd):/opt/unbound/etc/unbound klutchell/unbound
```

## Check DNS

Let's run a query for the baseline:
```bash
dig @127.0.0.1 -p 5300 adafruit.com
```

Which will show output resembling:
```dns-zone

; <<>> DiG 9.10.6 <<>> @127.0.0.1 -p 5300 adafruit.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 50330
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;adafruit.com.			IN	A

;; ANSWER SECTION:
adafruit.com.		300	IN	A	104.20.39.240
adafruit.com.		300	IN	A	104.20.38.240

;; Query time: 67 msec
;; SERVER: 127.0.0.1#5300(127.0.0.1)
;; WHEN: Sun Jan 19 05:38:29 PST 2020
;; MSG SIZE  rcvd: 73
```

The result confirms the ability of the Unbound container to resolve the
domain `adafruit.com` into its address record `104.20.39.240`


## Verify Blocking

Now let's see that Unbound blocks known advertiser domains.

```bash
dig @127.0.0.1 -p 5300 doubleclick.net
```

Which returns:
```dns-zone

; <<>> DiG 9.10.6 <<>> @127.0.0.1 -p 5300 doubleclick.net
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 61234
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;doubleclick.net.		IN	A

;; ANSWER SECTION:
doubleclick.net.	3600	IN	A	0.0.0.0

;; Query time: 2 msec
;; SERVER: 127.0.0.1#5300(127.0.0.1)
;; WHEN: Sun Jan 19 05:53:59 PST 2020
;; MSG SIZE  rcvd: 60

```

This result is what we want. Advertiser domains become mapped to `0.0.0.0`
 and NOT to their actual address. :tada:

