# Introduction

## Pi-hole, but pared down?

The first question you have after assembling your Raspi is easily,
"What can we run?" There's no shortage of projects. In my case,
I had one in mind before purchase time. It was [Brent Rubell's Pi-hole Ad Detection](https://learn.adafruit.com/pi-hole-ad-pitft-tft-detection-display)
 on Adafruit.

The idea behind the Pi-hole is to prevent ads from loading.
[Jacob Salmela also has a walk through of the entire process](https://jacobsalmela.com/2015/06/16/block-millions-ads-network-wide-with-a-raspberry-pi-hole-2-0/).
 It's a good description of how a web
page actually loads content, and dispels any lingering
mystery about browsing.

In following the tutorial, I saw that Dnsmasq is a key
component. This was cool to learn because I remember configuring
 Dnsmasq from the Archlinux docs under the section for filtering
 domains. Further scanning of the
guides led to details of an [Unbound combination for recursive DNS](https://docs.pi-hole.net/guides/unbound/#setting-up-pi-hole-as-a-recursive-dns-server-solution).
 This configuration stayed on my mind as I
played with different recipes, and cemented later as the 
most minimal for my purposes. To clarify, my idea of the sinkhole is a
 configuration free of a web server, DHCP, stats or logging.


## Goal

Again, the objective is to prevent ads. The other benefits cited
 stem from the elimination of ads:
- Reduce security risk of [cross-site scripts in ads](https://dlaa.me/blog/post/skyhole)
- [Save bandwidth](https://blog.codinghorror.com/an-exercise-program-for-the-fat-web/) on device data plans
- Stop providers from collecting your [DNS history](https://docs.pi-hole.net/guides/unbound/#the-problem-whom-can-you-trust)
