# Subsystems

## Diagram

![high level, simplified, diagram of the ad blocking method](../img/raspi-sinkhole.svg)

Blue dashed path represent traditional DNS resolution.
 Yellow solid path highlights the ad blocking.


## Modules

- Raspberry Pi
- Ubuntu Server arm64 image
- Unbound
- DNS zones conversion
