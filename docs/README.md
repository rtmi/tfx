---
navbar: false
home: true
heroText: Raspi DNS Sinkhole
tagline: fight the ads


footer: 2020 興怡
archiveText: archive
archiveLink: /2019.html
---

## [Easiest](/ezunbound/)
[&#9655;](/ezunbound/)
 See ad blocking magic in action for yourself!
 Just use the ready-made Docker image and zone list
 (klutchell/unbound + oznu/dns-zone-blacklist).

## [Modify](/sinkhole/)
[&#9634;](/sinkhole/) Learn from the zone
 redirect, and NXDOMAIN documentation to build your own custom Docker image.

## [DIY](/diyraspi/) appliance
[&#9711;](/diyraspi/) In which you are excited to assemble Raspi parts together,
 format micro SDs, install OS, and edit countless configuration files.

